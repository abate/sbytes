(* Copyright 2018 Vincent Bernardoff, Marco Stronati.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE. *)

external wipe : Bigstring.t -> unit =
  "ml_crypto_wipe" [@@noalloc]
external wipe_bytes : bytes -> unit =
  "ml_crypto_wipe_bytes" [@@noalloc]
external mlock : Bigstring.t -> int =
  "ml_mlock" [@@noalloc]
external munlock : Bigstring.t -> int =
  "ml_munlock" [@@noalloc]

let mlock buf = mlock buf = 0
let munlock buf = munlock buf = 0

let finalize f g = try let res = f () in g (); res with exn -> g (); raise exn

let wipe = wipe
let wipe_bytes = wipe_bytes
let wipe_string s = wipe_bytes (Bytes.unsafe_of_string s)

let with_wipe buf ~f =
  finalize
    (fun () -> f buf)
    (fun () -> wipe buf)

let with_wipe_lwt buf ~f =
  Lwt.finalize
    (fun () -> f buf)
    (fun () -> wipe buf ; Lwt.return_unit)

let with_wipe_bytes buf ~f =
  finalize
    (fun () -> f buf)
    (fun () -> wipe_bytes buf)

let with_wipe_bytes_lwt buf ~f =
  Lwt.finalize
    (fun () -> f buf)
    (fun () -> wipe_bytes buf ; Lwt.return_unit)

let with_wipe_string buf ~f =
  finalize
    (fun () -> f buf)
    (fun () -> wipe_string buf)

let with_wipe_string_lwt buf ~f =
    Lwt.finalize
        (fun () -> f buf)
            (fun () -> wipe_string buf ; Lwt.return_unit)

