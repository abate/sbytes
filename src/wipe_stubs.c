/* Copyright 2018 Vincent Bernardoff, Marco Stronati.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <caml/mlvalues.h>
#include <caml/bigarray.h>

/* https://monocypher.org/ */
void crypto_wipe(void *secret, size_t size)
{
    volatile uint8_t *v_secret = (uint8_t*)secret;
    for (int i=0; i<size; i++) {
        v_secret[i] = 0;
    }
}

CAMLprim value ml_crypto_wipe(value secret) {
    crypto_wipe(Caml_ba_data_val(secret),
                Caml_ba_array_val(secret)->dim[0]);
    return Val_unit;
}

CAMLprim value ml_crypto_wipe_bytes(value secret) {
    crypto_wipe(Bytes_val(secret),
                caml_string_length(secret));
    return Val_unit;
}

#include <sys/mman.h>

CAMLprim value ml_mlock(value buf) {
    return Val_int(mlock(Caml_ba_data_val(buf),
                         Caml_ba_array_val(buf)->dim[0]));
}

CAMLprim value ml_munlock(value buf) {
    return Val_int(munlock(Caml_ba_data_val(buf),
                           Caml_ba_array_val(buf)->dim[0]));
}

